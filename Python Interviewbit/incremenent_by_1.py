def plusOne(A):
    size = len(A)
    flag = True
    check = 0
    i = -1

    for element in A:
        if element != 0:
            check = element
            break

    A = A[A.index(check):]

    if A == []:
        return [1]

    size = len(A)

    while i >= -size:
        if A[i] + 1 == 10:
            A[i] = 0
        else:
            A[i] += 1
            flag = False
            break

        i -= 1

    if flag:
        A.append(1)
        A.sort(reverse=True)

    return A

print(plusOne([ 0, 6, 0, 6, 4, 8, 8, 1 ]))