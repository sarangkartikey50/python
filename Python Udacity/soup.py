# TODO: Implement the continue_crawl function described above
import requests
from bs4 import BeautifulSoup
from time import sleep
import urllib


def continue_crawl(search_history, target_url, max_steps=24):
    print("please wait... fetching page!")
    html = requests.get(search_history[-1])
    soup = BeautifulSoup(html.text, 'html.parser')

    content_div = soup.find(id="mw-content-text").find(class_="mw-parser-output")
    for element in content_div.find_all("p", recursive=False):
        if element.find("a", recursive=False):
            new_url = element.find("a", recursive=False).get('href')
            break

    if not new_url:
        print("no new url!")
        return False

    new_url = urllib.parse.urljoin('https://en.wikipedia.org/', new_url)

    if target_url == new_url:
        search_history.append(new_url)
        print("reached to target url")
        return False
    elif len(search_history) >= max_steps:
        search_history.append(new_url)
        print("more than 25 elements!")
        return False
    elif new_url in search_history:
        print("there is a loop")
        return False
    else:
        search_history.append(new_url)
        return True


search_history = ['https://en.wikipedia.org/wiki/Special:Random']
target_url = 'https://en.wikipedia.org/wiki/Philosophy'

while continue_crawl(search_history, target_url):
    print(search_history[-1])
    print("in looop")
    sleep(2)
