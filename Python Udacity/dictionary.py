population = {'sarang': 1, 'abha':2, 'kartikey': 3, 'khinoo': 4}
element = population.get('abha')
if element != None:
    print(element)
else:
    print("There is no such element.")


def most_prolific(songs):
    songs_counter = {}

    for song in songs:
        if songs[song] in songs_counter:
            songs_counter[songs[song]] += 1
        else:
            songs_counter[songs[song]] = 1

    # print(songs_counter)
    print(max(songs_counter, key=songs_counter.get))


Beatles_Discography = {"Please Please Me": 1963, "With the Beatles": 1963,
                       "A Hard Day's Night": 1964, "Beatles for Sale": 1964, "Twist and Shout": 1964,
                       "Help": 1965, "Rubber Soul": 1965, "Revolver": 1966,
                       "Sgt. Pepper's Lonely Hearts Club Band": 1967,
                       "Magical Mystery Tour": 1967, "The Beatles": 1968,
                       "Yellow Submarine": 1969, 'Abbey Road': 1969,
                       "Let It Be": 1970}

most_prolific(Beatles_Discography)