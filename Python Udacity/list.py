sample_string  = "Hi! My name is sarang kartikey"
print(sample_string[5:])
print("sarag" in sample_string)

sample_list = ["sarang", "kartikey", "abha", "khinoo"]
print(sample_list[2:])
print("sarang" in sample_list)

sample_list[2] = "Abha"
print sample_list

#sample_string[5] = "hey"
print sample_string

#join()
#it is used to join elements of list with the help of particular character

print("*".join(["sarang", "kartikey"]))
#join works for string only