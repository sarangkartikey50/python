def readable_timedelta(days):
    result = "{} week(s) and {} day(s).".format(str(days//7), str(days%7))
    return result

print(readable_timedelta(5))


def which_prize(points):
    prize = points
    if prize:
        print("some prize")
    else:
        print("No prize")

which_prize(None)
which_prize(5)    
