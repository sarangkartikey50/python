def pattern(input_string):
    input_map = {}
    result = ""

    for element in input_string:
        if element in input_map:
            input_map[element] += 1
        else:
            input_map[element] = 1

    for element in input_map:
        result += element + str(input_map[element])

    return result

print(pattern("bbbcccaaa"))