##len()
##len is used to find length of string. It doesn't work with types other than string

print(len("sarang"))
##print(len(45)) this will throw error

##type()
##this function returns the type of variable
print(type(45))
print(type(45.0))
print(type("45"))

##title()
##this function is used to make a string title i.e., all the first characters of each word will be capital.
##capitalize() works same as title()
print("this is a title".title())

##islower()
if("sarang Kartikey".islower()):
    print("lower case")
else :
    print("upper case")

##print(12.1.islower()) this will throw an error since islower() works with string only.


##count()
##this will return the no.of occurences of a sub-string in a string.

print("sarang kartikey is very hot. sarang kartikey is very cool. sarang is so cute.".count("sarang kartikey"))
print("sarang kartikey is very hot. sarang kartikey is very cool. sarang is so cute.".count("kartikey"))
print("sarang kartikey is very hot. sarang kartikey is very cool. sarang is so cute.".count("sarang"))

