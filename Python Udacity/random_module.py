import random

def password_generator(words):
    return str().join(random.sample(words, 3))

print(password_generator(['hello', 'world', 'how', 'are', 'you']))