def calculate(inp):
    numbers_string = inp.split(",")
    numbers = []
    for number in numbers_string:
        numbers.append(float(number))

    numbers.sort(reverse=True)
    result = (numbers[0]**2 + numbers[1]**2) - (numbers[1]**2 + numbers[2]**2)
    return result

inp = input("Enter three numbers seperated by ',' : ")
print("result is {}".format(calculate(inp)))