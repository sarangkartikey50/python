
import math

prime_factors_array = []

def find_prime_factors(number):
    prime_factors = set()

    while   (number % 2) == 0:
        prime_factors.add(2)
        prime_factors_array.append(2)
        number /= 2

    count = 3
    while count < math.sqrt(number):
        while   (number%count) == 0:
            number /= count
            prime_factors.add(count)
            prime_factors_array.append(count)

        count += 2

    if(number > 2):
        prime_factors.add(number)
        prime_factors_array.append(number)

    return prime_factors

check_set = set()
number = input("Enter a number - ")
check_set = find_prime_factors(int(number))

print("\nset of prime factors\n")
result = ""

for element in check_set:
    result += str(element) + " "

print(result)
result = ""

print("\narray of prime factors\n")
for element in prime_factors_array:
    result += str(element) + " "

print(result)


