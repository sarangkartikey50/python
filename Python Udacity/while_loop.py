def nearest_square(square):
    number = 0
    while (number+1) ** 2 <= square:
        number += 1

    return number ** 2

print(nearest_square(200))